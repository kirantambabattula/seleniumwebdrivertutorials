import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LaunchBrowser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Step1: 		
		WebDriverManager.chromedriver().setup(); //automatically webdriver will download driver files
		
		/*WebDriverManager.firefoxdriver().setup();
		
		WebDriverManager.iedriver().setup();
		
		WebDriverManager.edgedriver().setup();*/
		
		
		//Step2: 
		WebDriver wd = new ChromeDriver();
		//Runtime polymorphism --- Late Binding
		//Launches the browser
		
		/*WebDriver wd1 = new FirefoxDriver(); 
		WebDriver wd2 = new InternetExplorerDriver();*/
		
		//Step3:
		wd.get("https://www.google.com");
		String url = wd.getTitle();
		System.out.println(url);
		//https : 443 port
		//http: 80 port
		//ssh: 20 port
		// the java will wait until the browser loads all the objects, context completely
		/*wd1.get("https://www.facebook.com");
		wd2.get("https://www.amazon.com");*/


	}

}
  