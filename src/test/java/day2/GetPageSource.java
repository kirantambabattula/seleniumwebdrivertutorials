package day2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class GetPageSource {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriverManager.chromedriver().setup();
		
		WebDriver wd = new ChromeDriver();
		wd.get("https:\\www.google.com");
		String url = wd.getCurrentUrl();
		System.out.println(url);
		String title = wd.getTitle();
		System.out.println(title);
		String pgSrc = wd.getPageSource();
		System.out.println(pgSrc);
		
		
		

	}

}
